require 'ruble'

template t(:php_template) do |t|
  t.filetype = "*.php"
  t.location = "templates/template.php"
end

template "Athlon Framework Basic Controller" do |t|
  t.filetype = "*_controller.php"
  t.location = "templates/ath_basic_controller.php"
end

template "Athlon Framework CMS Controller" do |t|
  t.filetype = "*_controller.php"
  t.location = "templates/ath_cms_controller.php"
end